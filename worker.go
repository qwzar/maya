package maya

import (
	"log"
	"time"
)

//Worker gag
type Worker struct {
	ID      int
	Timer   time.Duration
	Attempt int
	State   string
	Task    func() error
}

//Run gag
func (w *Worker) Run() {
	err := w.Task()
	if err != nil {
		log.Println(err)
		if w.Attempt > 0 {
			time.AfterFunc(5*time.Second, w.Run)
			w.Attempt--
		}
		return
	}
	if w.Timer > 0 {
		time.AfterFunc(w.Timer*time.Second, w.Run)
	}
}
