package maya

import (
	"log"
	"os"
	"os/signal"
)

//Config gag
type Config struct {
	ListenSpec string
}

//DaemonRun gag
func DaemonRun(cfg *Config) error {
	log.Printf("Starting, Daemon")
	waitForSignal()
	return nil
}

func waitForSignal() {
	keys := make(chan os.Signal, 1)
	signal.Notify(keys, os.Interrupt)
	s := <-keys
	log.Printf("Got signal: %v, exiting.", s)
}
