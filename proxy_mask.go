package maya

import (
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/PuerkitoBio/goquery"
)

//GetProxyList gag
func GetProxyList(country string, ch chan<- interface{}) {
	client := &http.Client{}

	rocket := ReqRocket{
		Method:  "GET",
		BaseURL: "https://free-proxy-list.net",
		Headers: map[string][]string{
			"User-Agent": []string{"ChoperDropper/455 CFNetwork/902.2 Darwin/17.7.0"},
		},
	}

	res, err := Fire(client, rocket)
	if err != nil {
		return
	}

	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return
	}

	doc.Find("#proxylisttable > tbody").Each(func(i int, tbody *goquery.Selection) {
		target := fmt.Sprintf("tr:contains('%s'):contains('yes')", country)
		tbody.Find(target).Each(func(i int, s *goquery.Selection) {
			ip := s.Find("td:nth-child(1)").Text()
			port := s.Find("td:nth-child(2)").Text()
			proxyStr := fmt.Sprintf("%s:%s", ip, port)
			ch <- proxyStr
		})
	})
	ch <- "done"
}

//HTTTPBin gag
type HTTTPBin struct {
	Args    interface{} `json:"args"`
	Headers struct {
		AcceptEncoding string `json:"Accept-Encoding"`
		Connection     string `json:"Connection"`
		Host           string `json:"Host"`
		UserAgent      string `json:"User-Agent"`
	} `json:"headers"`
	Origin string `json:"origin"`
	URL    string `json:"url"`
}

//CheckProxy gag
func CheckProxy(proxy interface{}, ch chan<- interface{}) {
	proxyURL, _ := url.Parse(proxy.(string))
	client := &http.Client{
		Timeout:   time.Second * 5,
		Transport: &http.Transport{Proxy: http.ProxyURL(proxyURL)},
	}

	rocket := ReqRocket{
		Method:   "GET",
		BaseURL:  "https://httpbin.org",
		Endpoint: "/get",
		Headers: map[string][]string{
			"User-Agent": []string{"ChoperDropper/455 CFNetwork/902.2 Darwin/17.7.0"},
		},
	}

	res, err := Fire(client, rocket)

	if err != nil {
		return
	}

	if (res != nil) && res.StatusCode == 200 {
		ch <- proxy
		defer res.Body.Close()
	}
}
