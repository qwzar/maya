package maya

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
)

func TestBazooka(t *testing.T) {
	client := &http.Client{}

	rocket := ReqRocket{
		Method:  "POST",
		BaseURL: "https://httpbin.org/post",
		QueryParams: map[string]string{
			"param":        "value",
			"second_param": "second value",
		},
		Headers: map[string][]string{
			"User-Agent": []string{"ChoperDropper/455 CFNetwork/902.2 Darwin/17.7.0"},
		},
	}

	req, err := Bazooka(rocket)
	if err != nil {
		t.Error("Bazooka err: ", err)
	}

	res, err := client.Do(req)
	if err != nil {
		t.Error("client.Do err: ", err)
	}

	defer res.Body.Close()

	data, err := ioutil.ReadAll(res.Body)

	fmt.Println(string(data))

	if err != nil {
		t.Error("Not response: ", err)
	}
}
