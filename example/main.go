package main

import (
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/qwzar/maya"
)

func main() {

	//maya.TestFoo()

	err := ReadConfig("./conf/maya.json")
	if err != nil {
		panic(err)
	}

	maya.TestFoo()

	cfg := maya.Config{}
	if err := maya.DaemonRun(&cfg); err != nil {
		log.Printf("Error in main(): %v", err)
	}
}

//ReadConfig gag
func ReadConfig(confPath string) error {
	data, err := ioutil.ReadFile(confPath)
	if err != nil {
		return err
	}

	fmt.Printf("%s\n", string(data))

	return nil
}
