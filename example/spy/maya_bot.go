package main

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"sync"
	"time"

	"gitlab.com/qwzar/maya"
)

//BroodMother gag
type BroodMother struct {
	proxy, user *maya.Honeycomb
}

func main() {
	mother := &BroodMother{}
	go mother.proxyMask()
	go mother.userfire()

	cfg := maya.Config{}
	if err := maya.DaemonRun(&cfg); err != nil {
		log.Printf("Error in main(): %v", err)
	}
}

//User gag
type User struct {
	ID    int
	Proxy string
	OAuth string
	*sync.Mutex
}

//Pool gag
type Pool []*User

var pool = Pool{}

//userfire gag
func (bm *BroodMother) userfire() {

	pool = append(pool, &User{1, "proxy", "oauth", &sync.Mutex{}})
	pool = append(pool, &User{2, "proxy2", "oauth2", &sync.Mutex{}})
	pool = append(pool, &User{3, "proxy3", "oauth3", &sync.Mutex{}})

	bm.user = &maya.Honeycomb{
		Task:   make(chan interface{}, 0),
		Result: make(chan interface{}, 0),
		Err:    make(chan interface{}, 0),
	}

	bm.user.BeeSpawn(1, func(id int, task interface{}, h *maya.Honeycomb) {
		proxy := <-bm.proxy.Result
		user := task.(*User)
		user.Lock()
		user.Proxy = proxy.(string)
		user.Unlock()
		h.Result <- user
	})

	go func(name string) {
		for result := range bm.user.Result {
			user := result.(*User)
			RATATTATATA(user)
		}
	}("maya")

	go func() {
		for {
			for _, u := range pool {
				bm.user.Task <- u
			}
		}
	}()
}

func (bm *BroodMother) proxyMask() {

	bm.proxy = &maya.Honeycomb{
		Task:   make(chan interface{}, 0),
		Result: make(chan interface{}, 0),
		Err:    make(chan interface{}, 0),
	}

	bm.proxy.BeeSpawn(1, func(id int, task interface{}, h *maya.Honeycomb) {
		maya.CheckProxy(task, h.Result)
	})

	go func() {
		for {
			maya.GetProxyList("Russian Federation", bm.proxy.Task)
		}
	}()
}

//RATATTATATA gag
func RATATTATATA(user *User) {
	proxyURL, _ := url.Parse(user.Proxy)
	client := &http.Client{
		Timeout:   time.Second * 5,
		Transport: &http.Transport{Proxy: http.ProxyURL(proxyURL)},
	}

	rocket := maya.ReqRocket{
		Method:   "GET",
		BaseURL:  "https://httpbin.org",
		Endpoint: "/get",
		QueryParams: map[string]string{
			"id":    strconv.Itoa(user.ID),
			"OAuth": user.OAuth,
		},
		Headers: map[string][]string{
			"User-Agent": []string{"ChoperDropper/455 CFNetwork/902.2 Darwin/17.7.0"},
		},
	}

	res, err := maya.Turret(client, rocket)

	if err != nil {
		return
	}

	if (res != nil) && res.StatusCode == 200 {
		var result maya.HTTTPBin
		json.NewDecoder(res.Body).Decode(&result)
		log.Println(result.Args.(map[string]interface{})["OAuth"])
		log.Println(result.Origin)
		log.Println(result.Headers)
		defer res.Body.Close()
	}
}
