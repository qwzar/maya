package maya

import (
	"log"
	"sync"
	"time"
)

//Honeycomb gag
type Honeycomb struct {
	Bees   int
	Task   chan (interface{})
	Result chan (interface{})
	Err    chan (interface{})
	WG     sync.WaitGroup
}

//Work gag
type Work func(int, interface{}, *Honeycomb)

//BuffStatus gag
func (h *Honeycomb) BuffStatus() {
	for {
		log.Println("task buff:", len(h.Task), "result buff:", len(h.Result), "err buff:", len(h.Err))
		time.Sleep(time.Second * 1)
	}
}

//Run gag
func (h *Honeycomb) Run(id int, f Work) {
	for task := range h.Task {
		h.WG.Add(1)
		f(id, task, h)
		h.WG.Done()
	}
	defer func() {
		log.Println("die:", id)
	}()
}

//BeeSpawn gag
func (h *Honeycomb) BeeSpawn(c int, f Work) {
	h.Bees = c
	for i := 1; i <= c; i++ {
		go h.Run(i, f)
	}
}

//NewHoneycomb gag
func NewHoneycomb(taskBuff, resBuffer int) *Honeycomb {
	return &Honeycomb{
		Task:   make(chan interface{}, taskBuff),
		Result: make(chan interface{}, resBuffer),
		Err:    make(chan interface{}, resBuffer),
	}
}
