package maya

import (
	"time"
)

type job struct {
	Month, Day, Weekday  int8
	Hour, Minute, Second int8
	Task                 func(time.Time)
}

const ANY = -1 // mod by MDR

var jobs []job

// NewCronJob gag
func NewCronJob(month, day, weekday, hour, minute, second int8, task func(time.Time)) {
	cj := job{month, day, weekday, hour, minute, second, task}
	jobs = append(jobs, cj)
}

// NewMonthlyJob gag
func NewMonthlyJob(day, hour, minute, second int8, task func(time.Time)) {
	NewCronJob(ANY, day, ANY, hour, minute, second, task)
}

// NewWeeklyJob gag
func NewWeeklyJob(weekday, hour, minute, second int8, task func(time.Time)) {
	NewCronJob(ANY, ANY, weekday, hour, minute, second, task)
}

// NewDailyJob gag
func NewDailyJob(hour, minute, second int8, task func(time.Time)) {
	NewCronJob(ANY, ANY, ANY, hour, minute, second, task)
}

func (cj job) Matches(t time.Time) (ok bool) {
	ok = (cj.Month == ANY || cj.Month == int8(t.Month())) &&
		(cj.Day == ANY || cj.Day == int8(t.Day())) &&
		(cj.Weekday == ANY || cj.Weekday == int8(t.Weekday())) &&
		(cj.Hour == ANY || cj.Hour == int8(t.Hour())) &&
		(cj.Minute == ANY || cj.Minute == int8(t.Minute())) &&
		(cj.Second == ANY || cj.Second == int8(t.Second()))

	return ok
}

func processJobs() {
	for {
		now := time.Now()
		for _, j := range jobs {
			// execute all our cron tasks asynchronously
			if j.Matches(now) {
				go j.Task(now)
			}
		}
		time.Sleep(time.Second)
	}
}

func init() {
	go processJobs()
}
