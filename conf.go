package maya

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/tidwall/gjson"
)

type Target struct {
	Key  string `json:"key"`
	Args string `json:"args"`
	Next string `json:"next"`
}

type Pool []*Target

func TestFoo() {
	client := &http.Client{}

	rocket := ReqRocket{
		Method:  "GET",
		BaseURL: "https://c-plus-plus-taec.c9users.io/just.json",
	}

	res, err := Fire(client, rocket)

	if err != nil {
		return
	}

	var arr = Pool{
		&Target{"models", "", "code"},
		&Target{"cars.#[model_id==%s]#.number", "code", ""},
	}

	if (res != nil) && res.StatusCode == 200 {

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			panic(err.Error())
		}
		bodyString := string(body)
		go Reader(bodyString, "", arr, 0)
		defer res.Body.Close()
	}
}

//Boom gag
func Reader(r, b string, arr Pool, start int) {
	just := arr[start].Key

	if arr[start].Args != "" {
		just = fmt.Sprintf(arr[start].Key, b)
	}

	result := gjson.Get(r, just)
	result.ForEach(func(key, value gjson.Result) bool {
		if arr[start].Next != "" {
			code := value.Get(arr[start].Next).String()
			b = code
			fmt.Println("models:", b)
		}

		i := start
		i++

		if len(arr) > i {
			Reader(r, b, arr, i)
		} else {
			fmt.Println("here", value.String())
		}

		return true
	})
}

//ReadConfig gag
func ReadConfig(confPath string) error {
	data, err := ioutil.ReadFile(confPath)
	if err != nil {
		return err
	}

	fmt.Printf("%s\n", string(data))

	return nil
}
